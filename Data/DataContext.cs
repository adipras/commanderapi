using Commander.Models;
using Microsoft.EntityFrameworkCore;

namespace Commander.Data
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> opt) : base(opt)
        {
            
        }
        
        public DbSet<Command> Commands { get; set; }
    }
}